# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

# Log raw sensor data

import os
import asyncio
import numpy as np
from datetime import datetime

# Hardware abstration layer
# Barometric pressure sensor
from HAL.Barometer import Barometer
from HAL.hardware.BMP388 import BMP388

# Inertial measurement unit
from HAL.IMU import IMU
from HAL.hardware.LSM9DS1 import LSM9DS1

# Global Navigation Satellite System
from State.GNSS import GNSS


async def main():

    imu = IMU(LSM9DS1(1))

    baro = Barometer(BMP388(1))
    baro.start()

    gnss = GNSS('/dev/serial0')
    gnss.start()

    with open('log-' + str(datetime.now()) + '.csv', 'w') as outfile:

        # Write header
        outfile.write("time,ax,ay,az,wx,wy,wz,mx,my,mz,hb,hdotb,lat,lon,hg,gs,trk\n")

        # Main loop
        while True:

            t = datetime.utcnow()

            outfile.write(str(t) + ",")

            a = imu.getAcceleration()
            w = imu.getAngularVelocity()
            m = imu.getMagneticField()

            outfile.write(str(a[0]) + "," + str(a[1]) + "," + str(a[2]) + ",")
            outfile.write(str(w[0]) + "," + str(w[1]) + "," + str(w[2]) + ",")
            outfile.write(str(m[0]) + "," + str(m[1]) + "," + str(m[2]) + ",")

            outfile.write(str(baro.h) + "," + str(baro.hdot) + ",")

            if gnss.available:
                fix = gnss.readData()
                outfile.write(str(fix["latitude"]) + ",")
                outfile.write(str(fix["longitude"]) + ",")
                outfile.write(str(fix["altitude"]) + ",")
                outfile.write(str(fix["groundspeed"]) + ",")
                outfile.write(str(fix["track"]) + "\n")

            else:
                outfile.write(",,,,\n") # No new fix


            await asyncio.sleep(0.02 - (datetime.utcnow() - t).total_seconds()) # Up to50Hz


# Start asyncio loop
try:
    asyncio.ensure_future(main())
    asyncio.get_event_loop().run_forever()

except KeyboardInterrupt:
    pass

finally:
    asyncio.get_event_loop().close()
