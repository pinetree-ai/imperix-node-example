#!/bin/sh
sudo apt install -y python3-pip
pip3 install -r requirements.txt
pip3 uninstall numpy
sudo apt install -y python3-numpy libopenjp2-7 libtiff5 ffmpeg libatlas-base-dev python3-serial llvm

# For OpenCV
pip3 install opencv-contrib-python==4.1.0.25
sudo apt install libjasper-dev libilmbase-dev libopenexr-dev libgtk-3-dev libhdf5-dev libqt4-dev