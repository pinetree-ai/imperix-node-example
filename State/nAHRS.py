# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import time
import asyncio
import numpy as np

GZ = 9.81

# North-East-Down coordinate frame

# Naru's simplistic Attitude and Heading Reference System
class nAHRS:

    R = np.identity(3)

    roll = 0
    pitch = 0
    yaw = 0

    O = np.identity(3)


    def __init__(self, orient=None, kA=0.04, kH=0.12, kL=0.01):
        '''
        Constructor.
        
        Parameters
        ----------
        @param orient [np.ndarray] - coordinate frame rotation matrix
        @param kA [float] - variable correction coefficient gain
        @param kH [float] - max correction coefficient
        @param kL [float] - min correction coefficient
        '''
        # Initialize parameters
        self.kA = kA
        self.kH = kH
        self.kL = kL

        if orient is not None:
            self.O = orient

        self.t0 = time.time()


    # Start thread to process
    def start(self, imu, hz=50, enableMagnetometer=True):
        asyncio.ensure_future(self.coroutine(imu, hz, enableMagnetometer))
    

    # Coroutine to run in background
    async def coroutine(self, imu, hz, enableMagnetometer):

        while True: # Keep open

            try:
                if enableMagnetometer:
                    self.update(
                        imu.getAcceleration(),
                        imu.getAngularVelocity(),
                        imu.getMagneticField()
                    )

                else:
                    self.update(
                        imu.getAcceleration(),
                        imu.getAngularVelocity()
                    )

                await asyncio.sleep(1/hz)

            except Exception as e:
                print("AHRS error:", e)
                await asyncio.sleep(1) # Wait to try again

    
    def update(self, a, w, m=np.array([1,0,0], dtype=np.float64)):

        # Time update
        t = time.time()
        dt = t - self.t0
        self.t0 = t

        # Compute rotation matrix from accelerometer and magnetometer readings
        
        # Intermediary variables
        norm_a_sqr = np.dot(a,a)
        norm_a = np.sqrt(norm_a_sqr)

        # Get y axis
        # m roughly points North, a points Up
        y = np.cross(m, a)
        y /= np.linalg.norm(y)

        # z points down
        z = -a / np.linalg.norm(a)

        # Re-compute x axis
        x = np.cross(y,z)
        # x /= np.linalg.norm(x) # Redundant

        # Measurement matrix
        Rm = np.array([x])
        Rm = np.append(Rm, np.array([y]), axis=0)
        Rm = np.append(Rm, np.array([z]), axis=0)

        # Adaptive gain computation based on closeness of the acceleration vector to the expected gravity vector
        n = abs(norm_a_sqr - GZ*GZ)
        kN = np.clip(self.kA / n, self.kL, self.kH)

        # Perform state update
        dR = np.array([
            [0.0, -w[2], w[1]],
            [w[2], 0.0, -w[0]],
            [-w[1], w[0], 0.0]
        ], dtype=np.float64) * dt

        # Apply orientation transform
        Rm = Rm @ self.O
        dR = dR @ self.O

        self.R = Rm# * kN + (1 - kN) * (self.R + dR)

        # Re-normalize rotation matrix
        self.roll = np.arctan2(self.R[2,1], self.R[2,2])
        self.pitch = np.arctan2(-self.R[2,0], np.sqrt(self.R[2,1]*self.R[2,1] + self.R[2,2]*self.R[2,2]))
        self.yaw = np.arctan2(self.R[1,0], self.R[0,0])
        if self.yaw < 0:
            self.yaw += 2.0 * np.pi

        # Convert back to rotation matrix
        self.R[0,0] = np.cos(self.yaw) * np.cos(self.pitch)
        self.R[0,1] = np.sin(self.roll) * np.sin(self.pitch) * np.cos(self.yaw) - np.sin(self.yaw) * np.cos(self.roll)
        self.R[0,2] = np.sin(self.roll) * np.sin(self.yaw) + np.sin(self.pitch) * np.cos(self.roll) * np.cos(self.yaw)
        self.R[1,0] = np.sin(self.yaw) * np.cos(self.pitch)
        self.R[1,1] = np.sin(self.roll) * np.sin(self.yaw) * np.sin(self.pitch) + np.cos(self.roll) * np.cos(self.yaw) 
        self.R[1,2] = -np.sin(self.roll) * np.cos(self.yaw) + np.sin(self.yaw) * np.sin(self.pitch) * np.cos(self.roll)
        self.R[2,0] = -np.sin(self.pitch)
        self.R[2,1] = np.sin(self.roll) * np.cos(self.pitch)
        self.R[2,2] = np.cos(self.roll) * np.cos(self.pitch)
        