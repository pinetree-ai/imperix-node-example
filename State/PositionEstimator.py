# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import time
import asyncio
import quaternion
import numpy as np

from .KF import KalmanFilter

# North-East-Down refrence frame
# Position state: x,y,z,xd,yd,zd

# Estimator class
class PositionEstimator:

    t0 = 0
    A = np.eye(6, dtype=np.float64)
    Q = np.eye(6, dtype=np.float64)
    R = np.eye(6, dtype=np.float64)

    # Reference location
    lat0 = None
    lng0 = None
    h0 = None

    def __init__(self, hz=100, Sigma=0.1, sigmaGPSLoc=1.0, sigmaGPSVel=0.1, sigmaBaro=0.2):
        # Contructor

        # Initial state
        self.x = np.array([0.0,0.0,0.0,0.0,0.0,0.0])

        # Initial state covariance
        self.P = 10 * np.eye(6) # Start with a large positive-definite matrix... will converge over time

        # Initialize Kalman Filter
        # State process matrix
        self.A[0,3] = 1/hz
        self.A[1,4] = 1/hz
        self.A[2,5] = 1/hz

        # State covariance matrix - Update manually while in progress if needed
        self.Sigma = Sigma
        for i in range(3):
            self.Q[i,i] = 1/hz/hz * Sigma/2
            self.Q[i+3,i+3] = 1/hz * Sigma

        # Noise covariance matrix
        self.R[0,0] = sigmaGPSLoc**2
        self.R[1,1] = sigmaGPSLoc**2
        self.R[2,2] = sigmaBaro**2
        self.R[3,3] = sigmaGPSVel**2
        self.R[4,4] = sigmaGPSVel**2
        self.R[5,5] = sigmaBaro**2 # TODO - fix

        # Measurement observation matrix
        H = np.eye(6, dtype=np.float64) # Simple

        self.KF = KalmanFilter(self.x, self.P, self.A, self.Q, self.R, H)


    # Start thread to predict with AHRS, and correct with GPS+Baro 
    def start(self, ahrs, gnss, baro, hz=100):
        '''
        Start background process.

        Parameters
        ----------
        @param ahrs [xAHRS] - instantiated and started AHRS instance
        @param gnss [GNSS] - instantiated and started GNSS instance
        @param baro [Barometer] - instantiated and started Barometer instance
        @param hz [float] - sample rate
        '''

        asyncio.ensure_future(self.coroutine(ahrs, gnss, baro, hz))

    
    # Coroutine to run in background
    async def coroutine(self, ahrs, gnss, baro, hz):
        '''
        Process coroutine.

        Parameters
        ----------
        @param ahrs [xAHRS] - instantiated and started AHRS instance
        @param gnss [GNSS] - instantiated and started GNSS instance
        @param baro [Barometer] - instantiated and started Barometer instance
        @param hz [float] - sample rate
        '''

        while True: # Keep open
            
            try:
                t = time.time()
                self.dT = np.clip(t - self.t0, 0.001, 0.05)
                self.t0 = t

                # Correct acceleration from AHRS for gravity
                # Rotate gravitational field by attitude
                qg = ahrs.q.conjugate() * np.quaternion(0.0, 0.0, 0.0, -ahrs.G) * ahrs.q

                # Remove estimated gravity vector from filtered acceleration
                a = ahrs.a - np.array([ qg.x, qg.y, qg.z ], dtype=np.float64)

                # Process state
                self.A[0,3] = self.dT
                self.A[1,4] = self.dT
                self.A[2,5] = self.dT

                dT2_2 = self.dT**2 / 2.0
            
                dx = np.array([
                    dT2_2 * a[0],
                    dT2_2 * a[1],
                    dT2_2 * a[2],
                    self.dT * a[0],
                    self.dT * a[1],
                    self.dT * a[2]
                ], dtype=np.float64)

                for i in range(3):
                    self.Q[i,i] = dT2_2 * self.Sigma
                    self.Q[i+3,i+3] = self.dT * self.Sigma

                self.x = self.KF.predict(dx, self.A, self.Q)

                if self.h0 is None:
                    self.h0 = baro.h

                # self.x = self.KF.update(np.array([0.0,0.0,0.0,0.0,0.0,0.0]))

                # Correct when GPS data is available
                if gnss.available:

                    # Read available data from GPS
                    fix = gnss.readData()

                    if self.lat0 is None or self.lng0 is None:
                        self.lat0 = fix["latitude"]
                        self.lng0 = fix["longitude"]

                    v = fix["groundspeed"] * 0.514444
                    xdot = v * np.cos(np.radians(fix["track"]))
                    ydot = v * np.sin(np.radians(fix["track"]))

                    # Measurement vector
                    z = np.array([
                        (fix["latitude"] - self.lat0) * 111120.0,
                        (fix["longitude"] - self.lng0) * 111120.0,
                        (baro.h - self.h0) * 0.3048,
                        xdot,
                        ydot,
                        baro.hdot * 0.3048
                    ])

                    # Measurement update & correction
                    self.x = self.KF.update(z)


                # Correct altitude and vertical speed using barometer
                else:
                    
                    # TODO - Use H and R matrices for z-axis corrections only
                    pass


                await asyncio.sleep(1/hz - time.time() + t) # Up to hz

            except Exception as e:
                print("State Estimator error:", e)
                await asyncio.sleep(1) # Wait to try again...