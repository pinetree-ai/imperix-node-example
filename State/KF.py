# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import numpy as np

# Generalized Kalman Filter class
class KalmanFilter:

    def __init__(self, x0, P0, A, Q, R, H):
        '''
        Constructor.

        Parameters
        ----------
        @param x0 [np.ndarray] - (Nx1) initial state vector
        @param P0 [np.ndarray] - (NxN) initial state covariance matrix
        @param A [np.ndarray] - (NxN) state process matrix: x_k+1 = A * x_k + dx
        @param Q [np.ndarray] - (NxN) state noise covariance matrix
        @param R [np.ndarray] - (MxM) measurement noise covariance matrix
        @param H [np.ndarray] - (MxN) measurement observation matrix: z = H x
        '''

        # Initial state
        self.x = x0
        self.P = P0
        self.xhat = x0
        self.P0hat = P0
        
        # Initial process, noise covariances and observation matrices
        self.A = A
        self.Q = Q
        self.R = R
        self.H = H

    
    def predict(self, dx=None, A=None, Q=None):
        '''
        Make a state prediction. Pass A matrix and Q matrices if time-variant.

        Parameters
        ----------
        @param dx [np.ndarray] - (Nx1) vector of control influence to the state (not captured in A matrix)
        @param A [np.ndarray] - (NxN) state process matrix: x_k+1 = A * x_k + dx
        @param Q [np.ndarray] - (NxN) state noise covariance matrix

        Returns
        -------
        @out x [np.ndarray] - (Nx1) predicted state vector
        '''

        if dx is None:
            dx = np.zeros(self.x.shape)

        if A is not None:
            self.A = A

        if Q is not None:
            self.Q = Q

        # State process
        self.x = self.A @ self.x + dx

        # State covariance
        self.P = self.A @ self.P @ self.A.transpose() + self.Q

        return self.x

    
    def update(self, z, R=None, H=None):
        '''
        Perform measurement update. Pass R and H matrices if time-variant.

        Parameters
        ----------
        @param z [np.ndarray] - (Mx1) measurement vector
        @param R [np.ndarray] - (MxM) measurement noise covariance matrix
        @param H [np.ndarray] - (MxN) measurement observation matrix: z = H x

        Returns
        -------
        @out x [np.ndarray] - (Nx1) updated/corrected state vector
        '''

        if R is not None:
            self.R = R

        if H is not None:
            self.H = H

        # Compute Kalman Gain
        K = self.P @ self.H.transpose() @ np.linalg.inv( self.H @ self.P @ self.H.transpose() + self.R )

        # Make measurement update
        self.x += K @ ( z - self.H @ self.x )

        # Update covariance matrix
        self.P -= K @ self.H @ self.P

        return self.x

