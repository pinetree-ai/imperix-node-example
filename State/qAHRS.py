# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import time
import asyncio
import requests
import quaternion
import numpy as np
from datetime import date

# North-East-Down reference frame

# Quaternion-based AHRS using TGIC+KF
# Workflow:
# Accelerometer => Low-pass filter => f
# Magnetometer => Ellipse Hypothesis Compoensation => H
# (f,H) => Two-step Geometrically Intuitive Correction => q'
# (bias,Gyro) => Bias Correction => w
# (w,q') => KF => q
# (w,q) => Gyro Bias Computation => bias

# Implementation is a modified version of Kaiqiang Feng et. al's implementation:
# A New Quaternion-Based Kalman Filter for Real-Time Attitude Estimation Using the Two-Step Geometrically-Intuitive Correction Algorithm
# Published in MDPI Sensors Journal - 19 September, 2017

# Modifications:
# 1. Estimate gyroscope bias in a separate low-pass-filter and apply to sensor readings
# 2. Dynamic measurement noise covariance calculation using Jacobian
# 3. North-East-Down reference frame isntead of North-Up-East


# AHRS Class
class qAHRS: 

    # Default: Scottsdale, AZ
    G = 9.794 # m/s2 - Gravitational acceleration
    vgHat = np.array([ 0.0, 0.0, -1.0 ]) # What the accelerometer reading should be without external influence

    H = np.array([ 23.55, 4.191, 40.85 ]) # uT - Magnetic field
    Hmag = 47.338
    vmHat_xy = np.array([ 0.98453, 0.17522, 0.0 ]) # What the magnetometer reading should be without distortion


    # Coefficients
    kLPF_a = 0.1 # Accelerometer Low-pass filter gain
    kLPF_wBias = 0.00002 # Gyroscope bias computation LPF gain
    mu_a = 0.5 # External acceleration influence gain
    kA = 0.0 # Effect of acceleration deviation from G on measurement cov
    kM = 0.0 # Effect of magnetic field deviation from H on measurement cov

    a = np.array([0.0, 0.0, 0.0]) # Filtered accelerometer readings (acceleration)
    w = np.array([0.0, 0.0, 0.0]) # Bias-corrected gyroscope readings (angular velocities)
    wBias = np.array([0.0, 0.0, 0.0]) # Gyroscope bias estimates

    # State vector
    q = np.quaternion(1,0,0,0) # Unit quaternion, no rotation

    # Euler angle equivalents (ZYX sequence)
    roll = 0
    pitch = 0
    yaw = 0


    def __init__(self, lat=0, lon=0, alt=0, q0=None, Gamma=0.004, sigmaA=0.05, sigmaM=0.15, orient=None):
        '''
        Constructor.

        Parameters
        ----------
        @param lat [float] - latitude in degrees
        @param lon [float] - longitude in degrees
        @param alt [float] - altitude in meters (above Geoidal ellipsoid)
        @param q0 [np.quaternion] - initial alignment quaternion (if none, don't set)
        @param Gamma [float] - diagonal component of Gk @ Sigma_g @ Gk.transpose() for state noise covariance matrix Qk
        @param sigmaA [float] - accelerometer standard deviation in m/s2
        @param sigmaM [float] - magnetometer standard deviation in uT
        @param orient [np.ndarray] - coordinate frame rotation matrix
        '''

        self.t0 = time.time()

        # Fetch Earth model field values
        # self.fetchEarthModel(lat, lon, alt)

        # Initial state quaternion
        if q0 is not None:
            self.q = q0

        # State noise covariance parameter
        self.Gamma = Gamma
        self.sigmaA = sigmaA
        self.sigmaM = sigmaM

        # Initial state covariance matrix
        self.P = 10 * np.eye(4, dtype=np.float64) # Large positive definite matrix... will converge over time


    # fetch expected gravitational and magnetic field values
    def fetchEarthModel(self, lat=0, lon=0, alt=0):

        # Gravitational acceleration
        try:
            res = requests.get(f"https://geodesy.noaa.gov/api/gravd/gp?lat={lat}&lon={lon}&eht={alt}")

            self.G = res.json()['predictedGravity'] / 100000.0

            print("Expected gravitational acceleration:", self.G, "m/s2")

        except:
            pass

        # Magnetic field
        try:
            res = requests.get(f"http://geomag.bgs.ac.uk/web_service/GMModels/wmm/2020/?latitude={lat}&longitude={lon}&altitude={alt/1000.0}&date={date.today().isoformat()}&format=json")

            fieldValue = res.json()['geomagnetic-field-model-result']['field-value']

            self.H = np.array([
                fieldValue['north-intensity']['value'] / 1000.0,
                fieldValue['east-intensity']['value'] / 1000.0,
                fieldValue['vertical-intensity']['value'] / 1000.0
            ], dtype=np.float64)

            self.Hmag = np.linalg.norm(self.H)

            # Compute horizontal component for directional corrections
            H_xy = np.array([ self.H[0], self.H[1], 0.0 ])
            self.vmHat_xy = H_xy / np.linalg.norm(H_xy) # This is what the horizontal component of the magnetic field should be in nav-frame

            print("Expected total magnetic field intensity:", self.H, "uT")

        except:
            pass


    # Start thread to process
    def start(self, imu, hz=100, enableMagnetometer=True):
        asyncio.ensure_future(self.coroutine(imu, hz, enableMagnetometer))


    # Coroutine to run in background
    async def coroutine(self, imu, hz, enableMagnetometer):

        while True: # Keep open

            try:
                if enableMagnetometer:
                    self.update(
                        imu.getAcceleration(),
                        imu.getAngularVelocity(),
                        imu.getMagneticField()
                    )

                else:
                    self.update(
                        imu.getAcceleration(),
                        imu.getAngularVelocity()
                    )

                await asyncio.sleep(1/hz - self.dT)

            except Exception as e:
                print("AHRS error:", e)
                await asyncio.sleep(1) # Wait to try again


    # Update function
    def update(self, a, w, m=np.array([1.0, 0.0, 0.0])):

        # Time update
        t = time.time()
        self.dT = t - self.t0
        self.t0 = t

        # Apply gyroscope bias compensations
        w -= self.wBias

        # Apply low pass filter on accelerometer readings
        self.a = self.kLPF_a * a + (1.0 - self.kLPF_a) * self.a

        # Normalized measurement from the accelerometer -> measured gravity vector + external accel (noise-ish)
        vg = self.a / np.linalg.norm(self.a)


        # TGIC - Two-step Geometrically Intuitive Correction
        # Step 1 - Correct gravity vector using accelerometer readings
        # Estimate what the measured accelerometer reading should be
        qg = self.q * np.quaternion(0.0, self.a[0], self.a[1], self.a[2]) * self.q.conjugate()
        g = np.array([qg.x, qg.y, qg.z], dtype=np.float64)
        vg = g / np.linalg.norm(g)

        # This is what the gravitational vector should be
        vgHat = np.array([0.0, 0.0, -self.G]) / self.G

        # Error rotation angle
        self.dTheta_a = np.arccos( np.dot(vg, vgHat) )

        # Error rotation axis
        n_a = np.cross( vg, vgHat )
        n_a /= np.linalg.norm(n_a)

        # Accelerometer error quaternion
        s = self.mu_a * self.dTheta_a / 2.0 # Partial correction - converge averaging external influence to 0
        sin_s = np.sin(s)

        qError_a = np.quaternion(
            np.cos(s),
            n_a[0] * sin_s,
            n_a[1] * sin_s,
            n_a[2] * sin_s
        )

        # Measurement estimate
        q_a = qError_a * self.q


        # Step 2 - Correct magnetic field vector using magnetometer readings
        # Project magnetometer reading to xy-plane
        qm_xy = self.q * np.quaternion(0, m[0], m[1], m[2]) * self.q.conjugate()
        m_xy = np.array([ qm_xy.x, qm_xy.y, 0.0 ], dtype=np.float64) # Omit vertical/z component

        # Normalize
        vm_xy = m_xy / np.linalg.norm(m_xy)
        # Horizontal/planar component of the magnetic field must match up with vmHat_xy "constant" computed when fetching Earth model

        # Error rotation angle
        self.dTheta_m = np.arccos( np.clip(np.dot(vm_xy, self.vmHat_xy), -1.0, 1.0) )

        # Error rotation axis
        # Rotation is on the horizontal plane, so will always be about the z-axis
        n_m = np.cross( vm_xy, self.vmHat_xy )
        n_m /= np.linalg.norm(n_m)

        # Magnetometer error quaternion
        s = self.dTheta_m / 2.0
        sin_s = np.sin(s)

        qError_m = np.quaternion(
            np.cos(s),
            n_m[0] * sin_s,
            n_m[1] * sin_s,
            n_m[2] * sin_s
        )

        # Compute TGIC attitude quaternion based on level of magnetic distortion
        mmag = np.linalg.norm(m)

        # If there is significant magnetic distortion, don't use magnetometer-corrected attitude
        if abs( mmag - self.Hmag ) > 0.5 * abs(self.H[0]):
            qComp = q_a

        else:
            qComp = qError_m * q_a


        # Kalman Filter Implementation
        # State quaternion as numpy array/vector
        # Previous state
        qVec = np.array([self.q.w, self.q.x, self.q.y, self.q.z], dtype=np.float64)

        # Computed from measurement
        z = np.array([qComp.w, qComp.x, qComp.y, qComp.z], dtype=np.float64)

        # Noise covariance determinations
        # State noise covariance
        Q = self.dT * self.dT / 4.0 * self.Gamma * np.eye(4, dtype=np.float64)

        # Measurement noise covariance
        # R = self.computeR(mmag) # TODO - Implement dynamic covariance calculation
        R = np.eye(4, dtype=np.float64) * 0.001 # Temporary

        # Project State vector & covariance matrix
        # State process matrix:
        # Compute rate-change due to gyroscope angular velocities
        Omega = np.array([
            [ 0,    -w[0], -w[1], -w[2] ],
            [ w[0],     0,  w[2], -w[1] ],
            [ w[1], -w[2],    0,   w[0] ],
            [ w[2],  w[1], -w[0],     0 ]
        ], dtype=np.float64)

        exp = np.eye(4, dtype=np.float64) + 0.5 * Omega * self.dT

        # State projection
        qHat_pre = exp @ qVec

        # State covariance projection
        P_pre = exp @ self.P @ exp.transpose() + Q

        # Kalman Gain computation
        K = P_pre @ np.linalg.inv( P_pre + R )

        # Measurement update
        # State
        q = qHat_pre + K @ ( z - qHat_pre )

        self.q = np.quaternion(q[0], q[1], q[2], q[3])
        self.computeEuler() # Let's get these too..

        # Covariance
        self.P = ( np.eye(4,dtype=np.float64) - K ) * P_pre


        # TODO - This bias correction doesn't work very well... figure out a better solution. Should we include bias in the state and correct with KF?
        # Estimate gyroscope bias based on disparity between optimal estimate, and gyroscope estimate (qHat_pre)
        qHat = np.quaternion(qHat_pre[0], qHat_pre[1], qHat_pre[2], qHat_pre[3])

        # True error/difference between estimates
        dq = qHat * self.q.conjugate()

        # Get gyroscope rate bias estimate from difference
        dqAxis = np.array([ dq.x, dq.y, dq.z ])

        dwBias = 2.0 * np.arctan2( np.linalg.norm(dqAxis), dq.w ) * dqAxis / np.linalg.norm(dqAxis) / self.dT if np.linalg.norm(dqAxis) > 0 else 0

        # Use a highly attenuative low-pass filter to average bias out
        self.wBias = (1.0 - self.kLPF_wBias) * self.wBias + self.kLPF_wBias * (self.wBias - dwBias)


    # Compute Euler angles
    def computeEuler(self):

        q0 = self.q.w
        q1 = self.q.x
        q2 = self.q.y
        q3 = self.q.z

        self.roll = np.arctan2( 2 * ( q0*q1 + q2*q3 ), q0**2 - q1**2 - q2**2 + q3**2 )
        self.pitch = np.arcsin( np.clip( 2 * ( q0*q2 - q1*q3 ), -1.0, 1.0 ) )
        self.yaw = np.arctan2( 2 * ( q0*q3 + q1*q2 ), q0**2 + q1**2 - q2**2 - q3**2 )

        if self.yaw < 0:
            self.yaw += 2.0 * np.pi

'''
    # Get measurement noise covariance matrix
    def computeR(self, mmag):

        # Accelerometer
        SigmaA = (self.sigmaA**2 + self.kA * (np.linalg.norm(self.a) - self.G)**2) / self.G**2
        # Magnetometer
        SigmaM = (self.sigmaM**2 + self.kM * (mmag - self.Hmag)**2) / self.Hmag**2

        # Combined measurement covariance matrix (6x6)
        SigmaU = np.zeros((6,6), dtype=np.float64)
        SigmaU[0,0] = SigmaA
        SigmaU[1,2] = SigmaA
        SigmaU[2,2] = SigmaA
        SigmaU[3,3] = SigmaM
        SigmaU[4,4] = SigmaM
        SigmaU[5,5] = SigmaM

        q0 = self.q.w
        q1 = self.q.x
        q2 = self.q.y
        q3 = self.q.z

        mN = self.H[0]
        mU = self.H[1]
        mE = self.H[2]

        # Jacobian derived using helper
        J = np.array([[2*q3, 2*q2, 2*q1, 2*q0], [2*q0, -2*q1, 2*q2, -2*q3], [-2*q1, -2*q0, 2*q3, 2*q2], [-2*mE*q2 + 2*mN*q0 + 2*mU*q3, 2*mE*q3 + 2*mN*q1 + 2*mU*q2, -2*mE*q0 + 2*mN*q2 + 2*mU*q1, 2*mE*q1 + 2*mN*q3 + 2*mU*q0], [2*mE*q1 - 2*mN*q3 + 2*mU*q0, 2*mE*q0 + 2*mN*q2 - 2*mU*q1, 2*mE*q3 + 2*mN*q1 + 2*mU*q2, 2*mE*q2 - 2*mN*q0 - 2*mU*q3], [2*mE*q0 + 2*mN*q2 - 2*mU*q1, -2*mE*q1 + 2*mN*q3 - 2*mU*q0, -2*mE*q2 + 2*mN*q0 + 2*mU*q3, 2*mE*q3 + 2*mN*q1 + 2*mU*q2]], dtype=np.float64)

        return J.transpose() * SigmaU * J


# Helper function
def deriveJacobianUQ():

    import sympy as sp

    q0, q1, q2, q3 = sp.symbols('q0 q1 q2 q3')

    C = sp.Matrix([
        [ q0**2 + q1**2 - q2**2 - q3**2,    2*q0*q3 + 2*q1*q2,              2*q1*q3 - 2*q0*q2               ],
        [ 2*q1*q2 - 2*q0*q3,                q0**2 - q1**2 + q2**2 - q3**2,  2*q0*q1 + 2*q2*q3               ],
        [ 2*q0*q2 + 2*q1*q3,                2*q2*q3 - 2*q0*q1,              q0**2 - q1**2 - q2**2 + q3**2   ]
    ])

    a = sp.Matrix([0,1,0])
    aq = C * a


    mN, mU, mE = sp.symbols('mN mU mE')
    m = sp.Matrix([mN, mU, mE])

    mq = C * m

    uq = sp.Matrix([
        aq[0],
        aq[1],
        aq[2],
        mq[0],
        mq[1],
        mq[2]
    ])

    J = uq.jacobian([q0, q1, q2, q3])

    print(J)
'''