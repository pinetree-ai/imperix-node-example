# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import math
import serial
import asyncio

def isNumber(s):
    try:
        float(s)
        return True
    except:
        return False

# Global Navigation Satellite System
class GNSS:

    latitude = 0
    longitude = 0
    altitude = 0
    trackTrue = 0
    trackMag = 0
    groundspeed = 0
    nSat = 0
    mode = 'raw' # raw/dgps/rtk
    valid = False

    # Errors (m)
    sigmaLat = 0
    sigmaLng = 0
    sigmaAlt = 0

    # Vertical speed estimator
    verticalSpeed = 0
    prevAltitude = 0

    available = False # New data available? For use with readData function

    buffer = ''


    def __init__(self, port, baud=9600):

        self.serial = serial.Serial(port, baud)


    # Start thread to process
    def start(self, hz=20):
        asyncio.ensure_future(self.coroutine(hz))
    

    # Coroutine to run in background
    async def coroutine(self, hz):

        while True: # Keep open

            try:
                self.process()

                # Compute vertical speed
                self.verticalSpeed = hz * (self.altitude - self.prevAltitude)
                self.prevAltitude = self.altitude

                await asyncio.sleep(1/hz)

            except Exception as e:
                print("GNSS error:", e)
                await asyncio.sleep(1) # Wait to try again


    # Read and process
    def process(self):

        while self.serial.inWaiting() > 0:
            self.buffer += str(self.serial.read(), 'utf-8')

        if '\n' in self.buffer:
            segments = self.buffer.split('\n')

            # Process segments
            for segment in segments:

                components = segment.split(',')

                if len(components) < 2 or len(components[0]) != 6 or components[0][:2] != "$G":
                    continue # Skip

                # Vector track and ground speed
                if components[0][3:6] == "VTG" and len(components) > 7:

                    # Track w.r.t true north
                    if isNumber(components[1]):
                        self.trackTrue = float(components[1])

                    # Track w.r.t magnetic north
                    if isNumber(components[3]):
                        self.trackMag = float(components[3])

                    # Groundspeed (knots)
                    if isNumber(components[5]):
                        self.groundspeed = float(components[5])
                    

                # Essential fix data
                elif components[0][3:6] == "GGA" and len(components) > 11:
                    
                    if not isNumber(components[2]) or not isNumber(components[4]):
                        self.valid = False
                        continue

                    # Latitude
                    self.parseLat(components[2], components[3])

                    # Longitude
                    self.parseLng(components[4], components[5])

                    # Quality
                    if components[6] == '2':
                        self.mode = 'dgps'

                    elif components[6] == '4' or components[6] == '5':
                        self.mode = 'rtk'

                    else:
                        self.mode = 'raw'

                    # Number of satellites
                    if isNumber(components[7]):
                        self.nSat = int(components[7])

                    # Altitude
                    if isNumber(components[9]) and components[10] == 'M':
                        self.altitude = float(components[9]) * 3.28084

                        if isNumber(components[11]):
                            self.altitude -= float(components[11]) * 3.28084

                    elif isNumber(components[9]) and components[10] == 'F':
                        self.altitude = float(components[9])

                        if isNumber(components[11]):
                            self.altitude -= float(components[11])

                    self.available = True
                    self.valid = True


                # Latitude and Longitude
                elif components[0][3:6] == "GLL" and len(components) > 6:
                    
                    if components[6] == 'V':
                        self.valid = False
                        continue

                    # Latitude
                    self.parseLat(components[1], components[2])

                    # Longitude
                    self.parseLng(components[3], components[4])

                    self.available = True
                    self.valid = True


                # Recommended minimum
                elif components[0][3:6] == "RMC" and len(components) > 8:
                    
                    if components[2] == 'V':
                        self.valid = False
                        continue

                    # Latitude
                    self.parseLat(components[3], components[4])

                    # Longitude
                    self.parseLng(components[5], components[6])

                    # Groundspeed
                    if isNumber(components[7]):
                        self.groundspeed = float(components[7])

                    # True track
                    if isNumber(components[8]):
                        self.trackTrue = float(components[8])
                        
                    self.available = True
                    self.valid = True


                # Error statistics
                elif components[0][3:6] == "GST" and len(components) > 8:

                    if isNumber(components[6]):
                        self.sigmaLat = float(components[6])

                    if isNumber(components[7]):
                        self.sigmaLng = float(components[7])

                    if isNumber(components[8]):
                        self.sigmaAlt = float(components[8])

                # Don't process anything else...


            # Leave remaining in buffer
            self.buffer = segments[-1]


    def readData(self):

        self.available = False

        return {
            "latitude": self.latitude,
            "longitude": self.longitude,
            "altitude": self.altitude,
            "track": self.trackTrue,
            "groundspeed": self.groundspeed
        }


    # Helpers
    def parseLat(self, val, dir):
        if isNumber(val):
            self.latDeg = float(val)/100.0
            self.latMin = float(val)%100
            self.latitude = math.floor(self.latDeg) + self.latMin/60.0

            if dir != "N":
                self.latitude *= -1.0

    def parseLng(self, val, dir):
        if isNumber(val):
            self.lngDeg = float(val)/100.0
            self.lngMin = float(val)%100
            self.longitude = math.floor(self.lngDeg) + self.lngMin/60.0

            if dir != "E":
                self.longitude *= -1.0