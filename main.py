# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import os
import asyncio
import numpy as np
from datetime import datetime

from imperix import NodeLink

# Hardware abstration layer
# Barometric pressure sensor
from HAL.Barometer import Barometer
from HAL.hardware.BMP388 import BMP388

# Inertial measurement unit
from HAL.IMU import IMU
from HAL.hardware.LSM9DS1 import LSM9DS1

# Software data processors
# Attitude and Heading Reference System
from State.qAHRS import qAHRS

# Global Navigation Satellite System
from State.GNSS import GNSS

# Position estimator
# from State.PositionEstimator import PositionEstimator

# CV if video streaming is not supported
import cv2
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
cap.set(cv2.CAP_PROP_BUFFERSIZE, 1)

# Run `sudo pigpiod`
# Manual control command handler
async def manualControlHandler(c):

    pwmSteer = int(75000 + 40000 * float(c["STEER"]))
    os.system('pigs hp 13 50 ' + str(pwmSteer))

    pwmDrive = int(75000 + 30000 * float(c["DRIVE"]))
    os.system('pigs hp 18 50 ' + str(pwmDrive))


# Main function/coroutine
async def main():

    # Connect to streamer, authenticate, and start associated coroutines
    node = NodeLink(
        manualControlCallback=manualControlHandler
    )

    await node.connect()

    # Hardware Setup
    # Initialize IMU
    imu = IMU(LSM9DS1(1))

    # Calibrate IMU - if calibration doesn't already exist
    if not os.path.exists("imu.calib"):
        imu.calibrateAccelGyro()
        imu.calibrateMag()

    # Initialize AHRS with tuning parameters
    ahrs = qAHRS()
    ahrs.start(imu) # Start coroutine to poll data from IMU instance and process

    # Initialize Barometer
    baro = Barometer(BMP388(1))
    baro.start() # Start coroutine to process Barometer

    # Initialize GNSS
    gnss = GNSS('/dev/serial0')
    gnss.start()

    # Wait for AHRS to settle
    # await asyncio.sleep(3.0)

    # PE = PositionEstimator()
    # PE.start(ahrs, gnss, baro)

    # Streaming video using MPEGTS is much more efficient if encoding capability exists
    # await node.streamVideoFromSource("PRIMARY", "/dev/video0")

    # Main loop...
    while True:

        t = datetime.utcnow()

        await node.transmitImage(cv2.cvtColor(cap.read()[1], cv2.COLOR_BGR2RGB), feed="CV2.CAP1")

        # Transmit telemetry data
        await node.transmitTelemetry({
            # Required
            "TIMESTAMP": str(t),
            "ATT_PITCH": np.degrees(ahrs.pitch),
            "ATT_ROLL": np.degrees(ahrs.roll),
            "ATT_HEADING": np.degrees(ahrs.yaw),
            "LOC_LATITUDE": gnss.latitude,
            "LOC_LONGITUDE": gnss.longitude,
            # "LOC_ALTITUDE": gnss.altitude, # ft
            "LOC_ALTITUDE": baro.h, # ft
            "VEL_GROUNDSPEED": gnss.groundspeed * 0.514444, # m/s
            "VEL_AIRSPEED": 0,
            # "VEL_VERTICAL_SPEED": gnss.verticalSpeed,
            "VEL_VERTICAL_SPEED": baro.hdot, # ft/s
            "STS_BATTERY": 50,
            "STS_SIGNAL": 75,

            # Supplementary
            "LOC_VALID": gnss.valid,
            "LOC_NSAT": gnss.nSat,
            "ATT_TRACK": gnss.trackMag
        })

        # await node.transmitTelemetry({
        #     # Required
        #     "TIMESTAMP": str(t),
        #     "ATT_PITCH": np.degrees(ahrs.pitch),
        #     "ATT_ROLL": np.degrees(ahrs.roll),
        #     "ATT_HEADING": np.degrees(ahrs.yaw),
        #     "LOC_LATITUDE": (0 if PE.lat0 is None else PE.lat0) + PE.x[0] / 111120.0,
        #     "LOC_LONGITUDE": (0 if PE.lng0 is None else PE.lng0) + PE.x[1] / 111120.0,
        #     "LOC_ALTITUDE": (0 if PE.h0 is None else PE.h0) + PE.x[1] / 0.3048, # ft
        #     "VEL_GROUNDSPEED": np.sqrt(PE.x[3]**2 + PE.x[4]**2), # m/s
        #     "VEL_AIRSPEED": 0,
        #     "VEL_VERTICAL_SPEED": PE.x[5] / 0.3048, # ft/s
        #     "STS_BATTERY": 50,
        #     "STS_SIGNAL": 75,

        #     # Supplementary
        #     "LOC_VALID": gnss.valid,
        #     "LOC_NSAT": gnss.nSat,
        #     "ATT_TRACK": gnss.trackMag
        # })

        # Sleep for the remainder of the update interval
        await asyncio.sleep(0.05 - (datetime.utcnow() - t).total_seconds()) # 10Hz


# Start asyncio loop
try:
    asyncio.ensure_future(main())
    asyncio.get_event_loop().run_forever()

except KeyboardInterrupt:
    pass

finally:
    # Disable PWM
    os.system('pigs gp hp 50 0')
    asyncio.get_event_loop().close()
