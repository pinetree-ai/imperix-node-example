# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import time
import numpy as np

from .I2C import I2C


# Sensor class
class BMP388:

    # Temperature and pressure coefficients
    t = np.zeros(3)
    p = np.zeros(11)

    T = 0 # Temperature in Celcius
    P = 0 # Pressure in hPa

    def __init__(self, port, addr=0x77):
        '''
        Constructor.

        Parameters
        ----------
        @param port [int] - SMBus/I2C port index
        @param addr [int] - I2C device address
        '''

        self.bus = I2C(port, addr)

        try:

            # Verify chip
            whoami = self.bus.readByte(0x00)
            
            if whoami != 0x50:
                raise Exception(f"Barometer on port {port} did not identify as BMP388")

            # Compensation coefficients
            # Temperature
            self.t[0] = float(self.bus.readUInt16(0x31)) / 2**-8
            self.t[1] = float(self.bus.readUInt16(0x33)) / 2**30
            self.t[2] = float(self.bus.readInt8(0x35)) / 2**48

            # Pressure
            self.p[0] = (float(self.bus.readInt16(0x36)) - 2**14) / 2**20
            self.p[1] = (float(self.bus.readInt16(0x38)) - 2**14) / 2**29
            self.p[2] = float(self.bus.readInt8(0x3A)) / 2**32
            self.p[3] = float(self.bus.readInt8(0x3B)) / 2**37
            self.p[4] = float(self.bus.readUInt16(0x3C)) / 2**-3
            self.p[5] = float(self.bus.readUInt16(0x3E)) / 2**6
            self.p[6] = float(self.bus.readInt8(0x40)) / 2**8
            self.p[7] = float(self.bus.readInt8(0x41)) / 2**15
            self.p[8] = float(self.bus.readInt16(0x42)) / 2**48
            self.p[9] = float(self.bus.readInt8(0x44)) / 2**48
            self.p[10] = float(self.bus.readInt8(0x45)) / 2**65

            # Set pwoer control register to enable pressure and temperature in normal mode
            self.bus.writeByte(0x1B, 0b00110011)


        except Exception as e:
            print(e)
            raise Exception(f"Unable to initialize BMP388 on port {port}")


    def read(self):
        '''
        Read temperature and pressure. Use class variables T and P to get values.
        '''

        # Read temperature (3 bytes)
        Tdat = self.bus.readBlock(0x07, 3)
        Tadc = (Tdat[2] * 65536 + Tdat[1] * 256 + Tdat[0])
        
        # Read pressure (3 bytes)
        Pdat = self.bus.readBlock(0x04, 3)
        Padc = (Pdat[2] * 65536 + Pdat[1] * 256 + Pdat[0])

        # Apply compensation
        # https://ozzmaker.com/wp-content/uploads/2020/02/BMP388-ds001.pdf
        # Chapter 9
        pd1 = Tadc - self.t[0]
        pd2 = pd1 * self.t[1]

        # Compensated temperature
        T = pd2 + pd1 * pd1 * self.t[2]

        pd1 = self.p[5] * T
        pd2 = self.p[6] * T * T
        pd3 = self.p[7] * T * T * T
        po1 = self.p[4] + pd1 + pd2 + pd3

        pd1 = self.p[1] * T
        pd2 = self.p[2] * T * T
        pd3 = self.p[3] * T * T * T
        po2 = Padc * (self.p[0] + pd1 + pd2 + pd3)

        pd1 = Padc * Padc
        pd2 = self.p[8] + self.p[9] * T
        pd3 = pd1 * pd2
        pd4 = pd3 + Padc * Padc * Padc * self.p[10]

        # Compensated pressure
        P = po1 + po2 + pd4

        self.T = T
        self.P = P