# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import spidev

# SPI interface
class SPI:

    FLAG_READ_BYTE = 0x80
    FLAG_READ_BLOCK = 0x40

    # Constructor
    def __init__(self, port, dev):
        '''
        Constructor. Set FLAG_READ_BYTE or FLAG_READ_BLOCK manually if needed.

        Parameters
        ----------
        @param port [int] - SPI port
        @param dev [int] - SPI device number
        '''

        self.port = port
        self.dev = dev

        self.bus = spidev.SpiDev()


    # Destructor
    def __del__(self):
        self.close() # Close on destruct

    # Open device for read/write
    def open(self):
        self.bus.open(self.port, self.dev)
        self.bus.max_speed_hz = 10000000

    
    # Close device
    def close(self):
        self.bus.close()

    # Write register data
    def writeByte(self, reg, data):
       self.open()
       rx = self.bus.xfer2([reg, data])
       self.close()
       return rx

    # Read register byte
    def readByte(self, reg):
        self.open()
        rx = self.bus.xfer2([reg | self.FLAG_READ_BYTE, 0x00])
        self.close()
        return rx[1]

    # Read register block
    def readBlock(self, reg, length):
        self.open()
        tx = [0] * (length+1)
        tx[0] = reg | self.FLAG_READ_BLOCK
        rx = self.bus.xfer2(tx)
        self.close()
        return rx[1:len(rx)]

    # Read uint16
    def readUInt16(self, reg):
        data = self.readBlock(reg,2)
        return data[0] | data[1] << 8

    # Read int16
    def readInt16(self, reg):
        val = self.readUInt16(reg)
        return val if val < 32768 else val - 65536