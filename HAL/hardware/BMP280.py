# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import time
import numpy as np

from .I2C import I2C


# Sensor class
class BMP280:

    # Temperature and pressure coefficients
    t = np.zeros(3)
    p = np.zeros(9)

    T = 0 # Temperature in Celcius
    P = 0 # Pressure in hPa

    def __init__(self, port, addr=0x77):
        '''
        Constructor.

        Parameters
        ----------
        @param port [int] - SMBus/I2C port index
        @param addr [int] - I2C device address
        '''

        self.bus = I2C(port, addr)

        try:

            # Verify chip
            whoami = self.bus.readByte(0x00)
            
            if whoami != 0x58:
                raise Exception(f"Barometer on port {port} did not identify as BMP280")

            # Read 24 bytes from register 0x88
            coeff = self.bus.readBlock(0x88, 24)

            print(coeff)

            # Convert and set temperature and pressure coefficients
            # Temperature
            self.t[0] = coeff[1] * 256 + coeff[0]

            for i in range(1,3):
                self.t[i] = coeff[2*i+1] * 256 + coeff[2*i]
                if self.t[i] > 32767:
                    self.t[i] -= 65536

            # Pressure
            self.p[0] = coeff[7] * 256 + coeff[6]

            for i in range(1,9):
                self.p[i] = coeff[2*i+7] * 256 + coeff[2*i+6]
                if self.p[i] > 32767:
                    self.p[i] -= 63536

            # Set control measurement register to normal mode
            self.bus.writeByte(0xF4, 0x3F)


        except Exception as e:
            print(e)
            raise Exception(f"Unable to initialize BMP280 on port {port}")


    def read(self):
        '''
        Read temperature and pressure. Use class variables T and P to get values.
        '''

        # Read 8 bytes from data register
        d = self.bus.readBlock(0xF7, 8)

        # Convert pressure and temperature data to 19-bits
        Padc = (d[0] * 65536 + d[1] * 256 + d[2] & 0xF0) / 16
        Tadc = (d[3] * 65536 + d[4] * 256 + d[5] & 0xF0) / 16

        # Temperature offset calculations
        v1 = (Tadc / 16384.0 - self.t[0] / 1024.0) * self.t[1]
        v2 = (Tadc / 131072.0 - self.t[0] / 8192.0)**2 * self.t[2]

        Tfine = v1 + v2 # For pressure correction
        self.T = Tfine / 5120.0 # Temperature in Celcius

        # Pressure offset calculations
        v1 = Tfine / 2.0 - 64000.0
        v2 = v1 * v1 * self.p[5] / 32768.0
        v2 = v2 + v1 * self.p[4] * 2.0
        v2 = v2 / 4.0 + self.p[3] * 65536.0
        v1 = (self.p[2] * v1 * v1 / 524288.0 + self.p[1] * v1) / 524288.0
        v1 = (1.0 + v1 / 32768.0) * self.p[0]

        if v1 == 0:
            self.P = 0
            return

        p = 1048576.0 - Padc
        p = (p - v2 / 4096.0) * 6250.0 / v1
        
        v1 = self.p[8] * p * p / 2147483648.0
        v2 = p * self.p[7] / 32768.0

        self.P = (p + (v1 + v2 + self.p[6]) / 16.0) / 100.0
