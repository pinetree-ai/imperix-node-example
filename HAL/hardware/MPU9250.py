# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

from SPI import SPI

# Sensor class
class MPU9250:

    def __init__(self, port, spi=True, dev=1):
        '''
        Constructor.

        Parameters
        ----------
        @param port [int] - SMBus/I2C/SPI port
        @param spi [bool] - Use SPI instead of I2C?
        @param dev [int] - SPI device number or I2C address
        '''

        self.port = port
        self.dev = dev

        # TODO - Implement I2C
        self.bus = SPI(port, dev) if spi else None
            
        # Verify device connection
        try:
            pass

        except IOError:
            raise Exception('Unable to communicate with LSM9DS1')

        time.sleep(1.0)

        # Initialize IMU