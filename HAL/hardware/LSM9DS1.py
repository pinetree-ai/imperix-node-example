# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import time
import numpy as np

from .SPI import SPI
from .I2C import I2C


# North-East-Down coordinate frame

# Sensor class
class LSM9DS1:

    def __init__(self, port, spi=False):
        '''
        Constructor.

        Parameters
        ----------
        @param port [int] - SMBus/I2C/SPI port
        @param spi [bool] - use SPI interface? If not, use I2C
        '''

        # TODO - Implement SPI
        self.ag = None if spi else I2C(port, 0x6A)
        self.m = None if spi else I2C(port, 0x1C)

        # Connect and check for device
        try:
            whoamiAG = self.ag.readByte(0x0F)
            whoamiM = self.m.readByte(0x0F)

        except IOError:
            raise Exception('Unable to communicate with LSM9DS1')

        if whoamiAG != 0x68 or whoamiM != 0x3d:
            raise Exception(f"IMU sensor on port {port} did not identify as LSM9DS1")

        time.sleep(1.0)

        # Initialize IMU
        # Accelerometer
        self.ag.writeByte(0x1F, 0b00111000)   # Enable all axes
        self.ag.writeByte(0x20, 0b00111000)   # Set range to +/- 8g

        # Gyroscope
        self.ag.writeByte(0x1E, 0b00111000)   # Enable all axes
        self.ag.writeByte(0x10, 0b10111000)   # 476Hz ODR, set range to 2000 deg/s
        self.ag.writeByte(0x13, 0b10111000)   # Set orientation to match accelerometery

        # Magnetometer
        self.m.writeByte(0x20, 0b10011100)   # Enable temperature compensation, low power mode, 80Hz ODR
        self.m.writeByte(0x21, 0b01000000)   # Set range to +/- 12G
        self.m.writeByte(0x22, 0b00000000)   # Enable continuous update
        self.m.writeByte(0x23, 0b00000000)   # Low power mode for z-axis


    # TODO - Figure out why the axes are so f****d up

    # Sensor read functions
    def readA(self): # Result in m/s2

        return np.array([
            self.ag.readInt16(0x28),
            self.ag.readInt16(0x2A),
            -self.ag.readInt16(0x2C)
        ]).astype(np.float64) / 417.53

    
    def readG(self): # Result in rad/s

        return np.array([
            -self.ag.readInt16(0x18),
            -self.ag.readInt16(0x1A),
            self.ag.readInt16(0x1C)
        ]).astype(np.float64) / 938.73


    def readM(self): # Result in uT

        return np.array([
            -self.m.readInt16(0x28),
            self.m.readInt16(0x2A),
            -self.m.readInt16(0x2C)
        ]).astype(np.float64) / 27.3067