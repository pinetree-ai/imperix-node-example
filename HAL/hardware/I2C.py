# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import smbus

# I2C interface
class I2C:

    # Constructor
    def __init__(self, port, addr):
        '''
        Constructor.

        Parameters
        ----------
        @param port [int] - I2C port
        @param addr [int] - I2C address
        '''

        self.port = port
        self.addr = addr

        self.bus = smbus.SMBus(port)

    # Write register data
    def writeByte(self, reg, data):
       self.bus.write_byte_data(self.addr, reg, data)

    # Read register byte
    def readByte(self, reg):
        return self.bus.read_byte_data(self.addr, reg)

    # Read register block
    def readBlock(self, reg, length):
        return [ self.readByte(reg+i) for i in range(length) ]

    # Read int8
    def readInt8(self, reg):
        val = self.readByte(reg)
        return val if val < 128 else val - 256

    # Read uint16
    def readUInt16(self, reg):
        data = self.readBlock(reg,2)
        return data[0] | data[1] << 8

    # Read int16
    def readInt16(self, reg):
        val = self.readUInt16(reg)
        return val if val < 32768 else val - 65536

