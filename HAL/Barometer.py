# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import os
import time
import asyncio
import numpy as np


# Generalized Barometer class
class Barometer:

    # Loaded Barometer sensor object
    sensor = None
    # Sensor needs to implement methods: read
    # Sensor needs to have properties: T, P

    t0 = 0 # Last time step

    h = None # Filtered altitude
    hdot = None # Filtered vertical speed


    def __init__(self, sensor, kH=1):
        '''
        Constructor.

        Parameters
        ----------
        @param sensor - initialized sensor object
        @param kH [float] - low pass filter convergence time
        '''

        self.sensor = sensor
        self.kH = kH

        self.t0 = time.time()


    # Start thread to process
    def start(self, hz=20):
        asyncio.ensure_future(self.coroutine(hz))
    

    # Coroutine to run in background
    async def coroutine(self, hz):

        while True: # Keep open

            try:
                self.process()
                await asyncio.sleep(1/hz)

            except Exception as e:
                print("Barometer error:", e)
                await asyncio.sleep(1) # Wait to try again


    # Read and process
    def process(self):

        # Time update
        t = time.time()
        dt = t - self.t0
        self.t0 = t

        # Read sensor values
        self.sensor.read()

        # Timed filter coefficient
        kH = dt / self.kH
        h0 = self.h

        try:        

            # Hypsometric formula for altitude
            h = 3.28084 * ((101325 / self.sensor.P)**(1 / 5.257) - 1) * (self.sensor.T + 273.15) / 0.0065 

            # Apply low pass filter
            self.h = h if h0 is None else h0 * (1 - kH) + h * kH

        except:
            self.h = -1

        # Vertical speed
        if h0 is None or self.hdot is None:
            self.hdot = 0
            return

        hdot = (self.h - h0) / dt
        self.hdot = self.hdot * (1 - kH) + hdot * kH


    # Get Altitude
    def getAltitude(self):
        return self.h

    # Get vertical speed
    def getVerticalSpeed(self):
        return self.hdot
