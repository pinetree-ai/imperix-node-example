# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import os
import time
import json
import numpy as np


# North-East-Down coordinate frame

# Generalized IMU class
class IMU:

    # Loaded IMU sensor object
    sensor = None
    # sensor needs to implement methods: readA, readG, readM

    # Sensor bias offsets
    a0 = np.zeros((3,), dtype=np.float64)
    w0 = np.zeros((3,), dtype=np.float64)
    m0 = np.zeros((3,), dtype=np.float64)

    # Scale factors
    aSF = np.ones((3,), dtype=np.float64)
    wSF = np.ones((3,), dtype=np.float64)
    mSF = np.ones((3,), dtype=np.float64)


    def __init__(self, sensor, calibFile='imu.calib'):
        '''
        Constructor.

        Parameters
        ----------
        @param sensor - initialized sensor object
        @param calibFile [str] - calibration config file
        '''

        self.sensor = sensor
        self.calibFile = calibFile

        # If calibration file exists, load
        if not os.path.exists(self.calibFile):
            return

        with open(self.calibFile, 'r') as c:
            calib = json.load(c)

            # Load calibrations we can find
            if "a0" in calib:
                self.a0 = np.array(calib["a0"], dtype=np.float64)

                if "aSF" in calib:
                    self.aSF = np.array(calib["aSF"], dtype=np.float64)

                print("[IMU] Loaded accelerometer calibration")

            if "w0" in calib:
                self.w0 = np.array(calib["w0"], dtype=np.float64)

                if "wSF" in calib:
                    self.wSF = np.array(calib["wSF"], dtype=np.float64)

                print("[IMU] Loaded gyroscope calibration")

            if "m0" in calib:
                self.m0 = np.array(calib["m0"], dtype=np.float64)
                print("[IMU] Loaded magnetometer calibration")


    # Read converted and offset sensor values
    def getAcceleration(self):
        return (self.sensor.readA() - self.a0) * self.aSF

    def getAngularVelocity(self):
        return (self.sensor.readG() - self.w0) * self.wSF

    def getMagneticField(self):
        return self.sensor.readM() - self.m0


    # Update calibration file
    def saveCalibration(self, newCalib):
        try:
            # Load existing config to keep other components
            with open(self.calibFile, "r") as c:
                calib = json.load(c)
        except:
            calib = {}

        for key in newCalib:
            calib[key] = newCalib[key]

        with open(self.calibFile, "w") as c:
            json.dump(calib, c)

    
    # Calibration sequences
    # Accelerometer and gyroscope
    def calibrateAccelGyro(self):
        # Make sure device is upright (z-axis up)

        print()
        print("[IMU] Calibrating accelerometer and gyroscope.")
        print("[IMU] Keep device still and upright for three seconds...")

        time.sleep(1)

        self.a0 = np.array([0,0,0], dtype=np.float64)
        self.w0 = np.array([0,0,0], dtype=np.float64)

        # Average 50 readings
        for _ in range(50):
            self.a0 += self.sensor.readA()
            self.w0 += self.sensor.readG()
            time.sleep(0.02)

        self.a0 /= 50
        self.w0 /= 50

        # Compensate for gravity
        self.a0[2] += 9.81

        # Save calibration to file
        self.saveCalibration({
            "a0": self.a0.tolist(),
            "w0": self.w0.tolist()
        })

        print("[IMU] Calibration complete.")

    
    # Magnetometer
    def calibrateMag(self):
        # Move the device in a figure eight pattern

        print()
        print("[IMU] Calibrating magnetometer")
        print("[IMU] Move the device in a figure eight pattern for ten seconds...")
        
        ml = np.array([0,0,0], dtype=np.float64)
        mh = np.array([0,0,0], dtype=np.float64)

        for _ in range(500):
            m = self.sensor.readM()

            for i in range(3):
                if m[i] < ml[i]:
                    ml[i] = m[i]

                if m[i] > mh[i]:
                    mh[i] = m[i]

            time.sleep(0.02)

        self.m0 = (ml + mh) / 2.0

        # Save calibration to file
        self.saveCalibration({
            "m0": self.m0.tolist()
        })

        print("[IMU] Calibration complete.")
