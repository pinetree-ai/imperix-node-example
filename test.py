# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import os
import asyncio
import numpy as np

# Hardware abstration layer
# Barometric pressure sensor
from HAL.Barometer import Barometer
from HAL.hardware.BMP388 import BMP388

# Inertial measurement unit
from HAL.IMU import IMU
from HAL.hardware.LSM9DS1 import LSM9DS1

# Software data processors
# Attitude and Heading Reference System
from State.qAHRS import qAHRS

# Global Navigation Satellite System
from State.GNSS import GNSS

# Position estimator
from State.PositionEstimator import PositionEstimator


async def main():

    imu = IMU(LSM9DS1(1))
    # imu.calibrateAccelGyro()

    ahrs = qAHRS()
    ahrs.start(imu)

    # Let AHRS settle
    # await asyncio.sleep(3.0)

    baro = Barometer(BMP388(1))
    baro.start()

    gnss = GNSS('/dev/serial0')
    gnss.start()

    # PE = PositionEstimator()
    # PE.start(ahrs, gnss, baro)

    a = np.zeros((3,))

    for i in range(100):
        a += imu.getAcceleration()
        await asyncio.sleep(0.05)
        print(i)

    print(a / 100.0)


# Start asyncio loop
try:
    asyncio.ensure_future(main())
    asyncio.get_event_loop().run_forever()

except KeyboardInterrupt:
    pass

finally:
    asyncio.get_event_loop().close()

